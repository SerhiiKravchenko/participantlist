import { Injectable } from '@angular/core';
import { ParticipantId } from './participant';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

export class ThumbCacheItem {
    constructor(public readonly id: ParticipantId, public dataUrl: string, public readonly canvas: HTMLCanvasElement) {
    }
}

@Injectable()
export class ThumbCache {
    cache: {[id: string /* ParticipantId */]: ThumbCacheItem} = {};
    width: number;
    height: number;

    private _thumb$ = new Subject<ThumbCacheItem>();
    thumb$: Observable<ThumbCacheItem> = this._thumb$.asObservable();

    constructor() {
    }

    setThumbSize(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    addThumb(id: ParticipantId, dataUrl: string) {
        if (!dataUrl) {
            return;
        }

        let item = this.cache[id];
        if (item && item.dataUrl === dataUrl) {
            return;
        }

        if (item == null) {
            const canvas = <HTMLCanvasElement>document.createElement('canvas');
            canvas.width = this.width;
            canvas.height = this.height;
            item = new ThumbCacheItem(id, dataUrl, canvas);
            this.cache[id] = item;
        } else {
            item.dataUrl = dataUrl;
        }

        const image: HTMLImageElement = new Image;
        image.onload = () => {
            const context: CanvasRenderingContext2D = item.canvas.getContext('2d');
            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, this.width, this.height);
            this._thumb$.next(item);
            image.onload = undefined;
        };
        image.src = dataUrl;

        /*
        image.onload = () => {
            const context: CanvasRenderingContext2D = this.canvas.getContext('2d');
            context.drawImage(image, 0, 0, image.width, image.height, 0, 0, this.width, this.height);
            const dataURL = this.canvas.toDataURL();
            this.id2thumb[id] = dataURL;
            image.onload = undefined;
        };
        image.src = dataUrl;
        */

        // this.id2thumb[id] = dataUrl;
    }
}

