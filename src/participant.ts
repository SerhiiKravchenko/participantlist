import { createBig } from './utils';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { ParticipantFilter } from './participant-filter';


export type ParticipantId = string;

export class Participant {
    readonly id: ParticipantId;
    name: string;
    age: number;
    sex: boolean;
    haveDetails = false;
    picture: string;
    dataUrl: string;

    online: boolean;

    constructor(id: ParticipantId) {
        this.id = id;
    }

    static from(p: Participant): Participant {
        const instance = new Participant(p.id);
        instance.update(p);
        return instance;
    }

    update(p: Participant) {
        this.updateValues(p.name, p.online, p.age, p.sex, p.picture, p.dataUrl);
    }

    updateValues(name?: string, online?: boolean, age?: number, sex?: boolean, picture?: string, dataUrl?: string) {
        this.name = name;
        this.online = online;
        this.age = age;
        this.sex = sex;
        // this['buf'] = createBig();
        this.haveDetails = this.name != null;
        this.picture = picture;
        this.dataUrl = dataUrl;
    }
}

export class Participant$Visible {
    constructor(
        public participant$: BehaviorSubject<Participant>,
        public visible$: Observable<boolean>) {
    }
}
