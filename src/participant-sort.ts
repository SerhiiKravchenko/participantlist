import { Participant } from './participant';


/** must be synchronous with [[ParticipantSort.details]] */
export enum ParticipantSortBy {
    name = 0,
    age,
    sex,
    online,
    haveDetails
}

export class ParticipantSortByOrder {
    sortBy: ParticipantSortBy;
    ascending: boolean;

    static from(source: ParticipantSortByOrder) {
        const instance = new ParticipantSortByOrder(source.sortBy, source.ascending);
        return instance;
    }

    static isEqual(a: ParticipantSortByOrder, b: ParticipantSortByOrder) {
        if (a.sortBy !== b.sortBy) {
            return false;
        }
        if (a.ascending !== b.ascending) {
            return false;
        }

        return true;
    }

    constructor(sortBy: ParticipantSortBy, ascending: boolean) {
        this.sortBy = sortBy;
        this.ascending = ascending;
    }
}

export class ParticipantSortDetails {
    type: ParticipantSortBy;
    title: string;

    /** see [[ParticipantSort.details]] */
    compare: (a: Participant, b: Participant) => number;
}

export class ParticipantSort {
    static details: ParticipantSortDetails[] = [
        { type: ParticipantSortBy.name, title: 'name', compare: ParticipantSort.compareName },
        { type: ParticipantSortBy.age, title: 'age', compare: ParticipantSort.compareAge },
        { type: ParticipantSortBy.sex, title: 'sex', compare: ParticipantSort.compareSex },
        { type: ParticipantSortBy.online, title: 'online', compare: ParticipantSort.compareOnline },
        { type: ParticipantSortBy.haveDetails, title: 'haveDetails', compare: ParticipantSort.compareHaveDetails },
    ];

    sortByOrderList: ParticipantSortByOrder[] = [];

    static from(source: ParticipantSort) {
        const instance = new ParticipantSort;
        for (const s of source.sortByOrderList) {
            instance.sortByOrderList.push(ParticipantSortByOrder.from(s));
        }
        return instance;
    }

    static isEqual(a: ParticipantSort, b: ParticipantSort): boolean {
        if (a.sortByOrderList.length !== b.sortByOrderList.length) {
            return false;
        }
        const length = a.sortByOrderList.length;
        for (let i = 0; i < length; i++) {
            if (!ParticipantSortByOrder.isEqual(a.sortByOrderList[i], b.sortByOrderList[i])) {
                return false;
            }
        }

        return true;
    }

    protected static compareForNull(a: any, b: any): number {
        if (a == null) {
            if (b == null) {
                return 0;
            } else {
                return 1;
            }
        } else {
            if (b == null) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    protected static compareId(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.id, b.id);
        if (c !== 0) {
            return c;
        }
        if (a.id < b.id) {
            return -1;
        } else {
            if (a.id > b.id) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected static compareName(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.name, b.name);
        if (c !== 0) {
            return c;
        }
        if (a.name < b.name) {
            return -1;
        } else {
            if (a.name > b.name) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected static compareAge(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.age, b.age);
        if (c !== 0) {
            return c;
        }
        if (a.age < b.age) {
            return -1;
        } else {
            if (a.age > b.age) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected static compareSex(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.sex, b.sex);
        if (c !== 0) {
            return c;
        }
        if (a.sex < b.sex) {
            return -1;
        } else {
            if (a.sex > b.sex) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected static compareOnline(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.haveDetails, b.haveDetails);
        if (c !== 0) {
            return c;
        }
        if (a.online < b.online) {
            return -1;
        } else {
            if (a.online > b.online) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    protected static compareHaveDetails(a: Participant, b: Participant): number {
        const c = ParticipantSort.compareForNull(a.haveDetails, b.haveDetails);
        if (c !== 0) {
            return c;
        }
        if (a.haveDetails < b.haveDetails) {
            return -1;
        } else {
            if (a.haveDetails > b.haveDetails) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    getSortDirection(sortBy: ParticipantSortBy): boolean {
        for (const s of this.sortByOrderList) {
            if (s.sortBy === sortBy) {
                return s.ascending;
            }
        }
        return undefined;
    }

    add(sortByOrderNew: ParticipantSortByOrder) {
        const sortByOrderListNew: ParticipantSortByOrder[] = [sortByOrderNew];

        for (const sortByOrder of this.sortByOrderList) {
            if (sortByOrder.sortBy !== sortByOrderNew.sortBy) {
                sortByOrderListNew.push(sortByOrder);
            }
        }
        this.sortByOrderList = sortByOrderListNew;
    }

    compare(a: Participant, b: Participant): number {
        let result = 0;
        for (const sortByOrder of this.sortByOrderList) {
            const details: ParticipantSortDetails = ParticipantSort.details[sortByOrder.sortBy];
            result = details.compare(a, b);
            /*
                switch (sortByOrder.sortBy) {
                    case ParticipantSortBy.name: result = ParticipantSort.compareName(a, b);
                    break;
                    case ParticipantSortBy.age: result = ParticipantSort.compareAge(a, b);
                    break;
                    case ParticipantSortBy.age: result = ParticipantSort.compareSex(a, b);
                    break;
                    case ParticipantSortBy.haveDetails: result = ParticipantSort.compareHaveDetails(a, b);
                    break;
                }
            */
            if (!sortByOrder.ascending) {
                result = -result;
            }
            if (result !== 0) {
                break;
            }
        }

        if (result === 0) {
            result = ParticipantSort.compareId(a, b);
        }
        return result;
    }
}
