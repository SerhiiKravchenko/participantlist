
export function createBig() {
  const a = [];
  for (let i = 0; i < 0; i++) {
    a.push(4);
  }
  return a;
}

export function mergeUnique<T>(a: T[], b: T[]): T[] {
  const listSet = new Set<T>();
  for (const item of a) {
    listSet.add(item);
  }
  const listCombine = a.slice();
  for (const item of b) {
    if (!listSet.has(item)) {
      listCombine.push(item);
    }
  }

  return listCombine;
}

export function createCSSSelector(selector, style) {
  if (!document.styleSheets) {
    return;
  }
  if (document.getElementsByTagName('head').length === 0) {
    return;
  }

  let styleSheet, mediaType;

  if (document.styleSheets.length > 0) {
    const l = document.styleSheets.length;
    for (let i = 0; i < l; i++) {
      if (document.styleSheets[i].disabled) {
        continue;
      }
      const media: any = document.styleSheets[i].media;
      mediaType = typeof media;

      if (mediaType === 'string') {
        if (media === '' || (media.indexOf('screen') !== -1)) {
          styleSheet = document.styleSheets[i];
        }
      } else {
        if (mediaType === 'object') {
          if (media.mediaText === '' || (media.mediaText.indexOf('screen') !== -1)) {
            styleSheet = document.styleSheets[i];
          }
        }
      }

      if (typeof styleSheet !== 'undefined') {
        break;
      }
    }
  }

  if (typeof styleSheet === 'undefined') {
    const styleSheetElement = document.createElement('style');
    styleSheetElement.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(styleSheetElement);

    for (let i = 0; i < document.styleSheets.length; i++) {
      if (document.styleSheets[i].disabled) {
        continue;
      }
      styleSheet = document.styleSheets[i];
    }

    mediaType = typeof styleSheet.media;
  }

  if (mediaType === 'string') {
    const l = styleSheet.rules.length;
    for (let i = 0; i < l; i++) {
      if (styleSheet.rules[i].selectorText && styleSheet.rules[i].selectorText.toLowerCase() == selector.toLowerCase()) {
        styleSheet.rules[i].style.cssText = style;
        return;
      }
    }
    styleSheet.addRule(selector, style);
  } else {
      if (mediaType === 'object') {
        const styleSheetLength = (styleSheet.cssRules) ? styleSheet.cssRules.length : 0;
        for (let i = 0; i < styleSheetLength; i++) {
          if (styleSheet.cssRules[i].selectorText && styleSheet.cssRules[i].selectorText.toLowerCase() === selector.toLowerCase()) {
            styleSheet.cssRules[i].style.cssText = style;
            return;
          }
        }
        styleSheet.insertRule(selector + '{' + style + '}', styleSheetLength);
      }
    }
}
