
import { Observable, BehaviorSubject } from 'rxjs';
import { Participant, ParticipantId } from './participant';
import { Injectable } from '@angular/core';
import { ParticipantServer } from './participant-server';
import { ThumbCache } from './thumb-cache';

class CacheItem {
    static updateTimeout = 300000; // every 60 second

    readonly id: ParticipantId;
    timeRequest: Date;
    timeResponse: Date;
    readonly subject: BehaviorSubject<Participant>;
    priority = 0;

    constructor(id: ParticipantId) {
        this.id = id;
        this.subject = new BehaviorSubject<Participant>(new Participant(id)); // initialize first empty Participant
    }

    isNeedRefresh() {
        /*
        if (this.subject.observers.length === 0) { // no observers
            return false;
        }
        */
        if (this.timeRequest == null) { // no request for now
            return true;
        }
        if (Date.now() - this.timeRequest.getTime() > CacheItem.updateTimeout) {
            return true;
        }
        return false;
    }

    onFetchForRequest() {
        this.timeRequest = new Date;
    }

    update(participant: Participant) {
        if (this.id !== participant.id) {
            throw new Error(`update(participant: Participant): participant id: mismatch. '${this.id}'!='${participant.id}'`);
        }
        this.timeResponse = new Date;
        this.subject.next(participant);
    }
}

export class RequestPriority {
    constructor(public id: ParticipantId, public priority: number) {
    }
}
@Injectable()
export class ParticipantCache {
    static updateMaxCount = 50;

    updateInterval = 1000;

    protected cache: {[id: string]: CacheItem} = {}; // new Map<string, CacheItem>();
    priorityToTime = 3000;

    constructor(
        private server: ParticipantServer,
        private thumbCache: ThumbCache
    ) {
        this.schedulerRequest();
    }

    protected getCacheItem(
        id: ParticipantId
    ): CacheItem {
        let item = this.cache[id];
        if (!item) {
            item = new CacheItem(id);
            this.cache[id] = item;
        }
        return item;
    }

    requestParticipants(
        ids: ParticipantId[],
        urgent: boolean = false
    ): BehaviorSubject<Participant>[] {
        const participants = ids.map(id => this.getCacheItem(id).subject);
        if (urgent) {
            this.request();
        }
        return participants;
    }

    requestPriority(requestPriority: RequestPriority) {
        const item = this.getCacheItem(requestPriority.id);
        const now = Date.now();
        item.priority = now + requestPriority.priority * this.priorityToTime;
    }

    protected schedulerRequest() {
        setTimeout(() => this.request(), this.updateInterval);
    }

    protected request() {
        const all: CacheItem[] = [];
        for (const id of Object.keys(this.cache)) {
            const item = this.cache[id];
            if (item.isNeedRefresh()) {
                all.push(item);
            }
        }

        all.sort((a: CacheItem, b: CacheItem) => {
            if (a.priority < b.priority) {
                return 1;
            } else if (a.priority > b.priority) {
                return -1;
            } else if (a.id > b.id) {
                return 1;
            } else if (a.id < b.id) {
                return -1;
            } else {
                return 0;
            }
        });

        const forUpdate: ParticipantId[] = [];
        for (const item of all) {
            if (forUpdate.length >= ParticipantCache.updateMaxCount) {
                break;
            }
            item.onFetchForRequest();
            forUpdate.push(item.id);
        }
        const response$ = this.server.requestParticipants(forUpdate);
        response$.subscribe(participants => {
            this.update(participants);
        });
    }

    protected update(participants: Participant[]) {
        for (const participant of participants) {
            const cache = this.cache[participant.id];
            if (cache) {
                cache.update(participant);
            }
            this.thumbCache.addThumb(participant.id, participant.dataUrl);
        }
        this.schedulerRequest();
    }
}
