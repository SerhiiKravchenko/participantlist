import { Participant, ParticipantId } from './participant';
import { ParticipantCache } from './participant-cache';
import { Injectable } from '@angular/core';

import { Component } from '@angular/core/src/metadata/directives';
import { Observable, ReplaySubject, BehaviorSubject } from 'rxjs';
import { participantDetails } from './participant-list-sample';

@Injectable()
export class ParticipantServer {
    readonly lenght = 2000;
    chanceToChangeOnlineStatusPerSecond = 0.01;
    protected lastUpdate = new Date();
    protected id2participant = new Map<ParticipantId, Participant>();
    protected onlineList$ = new BehaviorSubject<ParticipantId[]>([]);
    updateInterval = 10000;

    constructor() {
        this.initList();
        this.schedulerUpdate();
    }

    initList() {
        for (let i = 0; i < this.lenght; i++) {
            const id = `id:[${i}]`;
            const details = participantDetails[Math.floor(Math.random() * participantDetails.length)];
            const name = details.name ? details.name : `name:[${i}]`;
            const online = Math.random() < 0.95;
            const age = Math.floor(Math.random() * 60) + 18;
            let sex: boolean;
            switch (Math.floor(Math.random() * 3)) {
                case 0: sex = true; break;
                case 1: sex = false; break;
                case 2: sex = undefined; break;
            }

            const participant = new Participant(id);
            participant.updateValues(name, online, age, sex, details.url, details.dataUrl);

            this.id2participant.set(id, participant);
        }
        this.update();
    }

    length(): number {
        return this.id2participant.size;
    }

    requestOnlineList(): Observable<ParticipantId[]> {
        return this.onlineList$;
    }

    requestParticipants(ids: ParticipantId[]): Observable<Participant[]> {
        const participants$ = new ReplaySubject<Participant[]>(null);
        const delayParticipants = participants$.
            delay(10); // faked network latency
        const list = ids.
            map(id => this.id2participant.get(id)).
            map(participant => {
                /*
                if (Math.random() < 0.5) {
                    const participantNew = Participant.from(participant);
                    participantNew.online = !participant.online;
                    this.id2participant.set(participantNew.id, participantNew);
                    participant = participantNew;
                }
                */
                // participantNew['buf'] = createBig(); // attach bit object, for detecting memory leaks
                return participant;
            });
        // participants$['buf'] = createBig();
        participants$.next(list);
        participants$.complete();
        return delayParticipants;
    }

    protected getOnlineList() {
        const onlineList: ParticipantId[] = [];
        this.id2participant.forEach(participant => {
            if (participant.online) {
                onlineList.push(participant.id);
            }
        });
        return onlineList;
    }

    schedulerUpdate() {
        setTimeout(() => this.update(), this.updateInterval);
    }

    update() {
        const now = new Date();
        const elapsedFromLastUpdate = this.lastUpdate ? now.getTime() - this.lastUpdate.getTime() : 0;
        this.lastUpdate = now;
        const chanceToChangeOnlineStatus = Math.min(elapsedFromLastUpdate * this.chanceToChangeOnlineStatusPerSecond / 1000, 1);
        this.id2participant.forEach(participant => {

            if (Math.random() < chanceToChangeOnlineStatus) {
                const participantNew = Participant.from(participant);
                participantNew.online = !participant.online;
                this.id2participant.set(participantNew.id, participantNew);
                participant = participantNew;
            }

        });
        const onlineList = this.getOnlineList();
        this.onlineList$.next(onlineList);
        this.schedulerUpdate();
    }
}
