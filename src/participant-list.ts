
import { Participant$Visible, Participant } from './participant';
import { ParticipantCache } from './participant-cache';
import { ParticipantFilter } from './participant-filter';
import { Observable } from 'rxjs';
import { ParticipantListVisualParameters } from './participant-list-visual-parameter';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export class ParticipantList {
    list: Participant$Visible[];
    online = 0;
    /** absolute position of filtered item in full list */
    id2position: {[id: string]: number} = {};

    constructor(
        list: BehaviorSubject<Participant>[],
        public visualParameters: ParticipantListVisualParameters,
        public scrollTop$: Observable<number>
    ) {
        const length = list.length;
        this.list = [];
        const listHeight = visualParameters.listHeight / visualParameters.participantHeight;
        const listMiddleY = listHeight / 2;
        for (let i = 0; i < length; i++) {
            const participant$ = list[i];
            const participant = participant$.value;
            this.id2position[participant.id] = i;
            if (participant.online) {
                this.online++;
            }

            const participantMiddleY = Math.floor(i / visualParameters.columnCount) + 0.5 - listMiddleY;
            const visible$ = scrollTop$.map(scrollTop => {
                const scrollTopItem = scrollTop / visualParameters.participantHeight;
                const distanceFromEdge = Math.abs(participantMiddleY - scrollTopItem) - listMiddleY;
                return distanceFromEdge < visualParameters.maxDistanceFromEdgeForVisibility;
            }).distinctUntilChanged();
            this.list.push(new Participant$Visible(
                participant$,
                visible$
            ));
        }
    }

    getParticipantByPosition(position: number): Participant {
        if (position >= 0 && position < this.list.length) {
            return this.list[position].participant$.value;
        } else {
            return null;
        }
    }
}
