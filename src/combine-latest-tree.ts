import { Observable } from 'rxjs';

export function combineLatestTree<T, Accum>(
    listOf$: Observable<T>[],
    addValue: (accum: Accum, value: T) => Accum,
    addAccum: (accum: Accum, value: Accum) => Accum,
    initialAccum: () => Accum,
    branchSize: number,
    debounceTime: number
): Observable<Accum> {
    if (listOf$.length <= branchSize) {
        // short branch, will have only leafs
        return Observable.combineLatest(listOf$, (...list: T[]) => {
            const reduce = list.reduce(addValue, initialAccum());
            return reduce;
        }).debounceTime(debounceTime);
    }

    let subBranch: Observable<T>[] = [];
    const branch: Observable<Accum>[] = [];
    const pushBranch = () => {
        if (subBranch.length > 0) {
            branch.push(combineLatestTree<T, Accum>(subBranch, addValue, addAccum, initialAccum, branchSize, debounceTime));
            subBranch = [];
        }
    };

    const totalBranchSize = Math.floor(listOf$.length / branchSize) + 1;
    for (const value$ of listOf$) {
        subBranch.push(value$);
        if (subBranch.length >= totalBranchSize) {
            pushBranch();
        }
    }
    pushBranch();

    return Observable.combineLatest(branch, (...list: Accum[]) => {
        const reduce = list.reduce(addAccum, initialAccum());
        return reduce;
    }).debounceTime(debounceTime);
}
