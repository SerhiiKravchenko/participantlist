
import { Participant } from './participant';

export class ParticipantFilter {
    name: string;
    ageMin: number;
    ageMax: number;
    male: boolean;
    female: boolean;
    hybrid: boolean;

    constructor(options: {
        name?: string,
        ageMin?: string,
        ageMax?: string,
        male?: boolean,
        female?: boolean,
        hybrid?: boolean
    } = {}) {
        this.name = options.name;
        this.ageMin = !options.ageMin || isNaN(Number(options.ageMin)) ? undefined : Number(options.ageMin);
        this.ageMax = !options.ageMax || isNaN(Number(options.ageMax)) ? undefined : Number(options.ageMax);
        this.male = options.male;
        this.female = options.female;
        this.hybrid = options.hybrid;
    }

    isFiltered(
        participant: Participant
    ): boolean {
        const filteredName = !this.name || (participant.name && participant.name.includes(this.name));
        const filteredAgeMin = this.ageMin == null || (participant.age && participant.age >= this.ageMin);
        const filteredAgeMax = this.ageMax == null || (participant.age && participant.age <= this.ageMax);
        const filtered =
            filteredName && filteredAgeMin && filteredAgeMax;
        return filtered;
    }

}
