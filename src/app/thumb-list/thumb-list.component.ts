
import { ThumbCache } from './../../thumb-cache';
import { ParticipantList } from './../../participant-list';

import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { ParticipantListVisualParameters } from '../../participant-list-visual-parameter';
import { ChangeDetectionStrategy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-thumb-list',
  templateUrl: './thumb-list.component.html',
  styleUrls: ['./thumb-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThumbListComponent implements OnInit, OnDestroy {
  _participantList: ParticipantList;
  canvas: HTMLCanvasElement;
  width: number;
  height: number;
  length: number;

  columns: number;
  rows: number;
  widthThumbOnCanvas: number;
  heightThumbOnCanvas: number;
  thumb$Subscription: Subscription;

  @Input()
  set participantList(_participantList: ParticipantList) {
    this._participantList = _participantList;
    if (_participantList == null) {
      return;
    }

    const visualParameters = _participantList.visualParameters;

    this.width = visualParameters.scrollBarWidth;
    this.height = visualParameters.listHeight;

    this.length = _participantList.list.length;

    const native: HTMLElement = this.element.nativeElement as HTMLElement;
    this.canvas = <HTMLCanvasElement>native.querySelector('.thumb-canvas');
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    const context: CanvasRenderingContext2D = this.canvas.getContext('2d');
    context.fillStyle = 'rgba(0, 0, 64, 0)';
    context.fillRect(0, 0, this.width, this.height);

    this.columns = 0;
    do {// iterate while find optimal number of columns and rows
      this.columns++;
      this.widthThumbOnCanvas = Math.floor(this.width / this.columns);
      this.heightThumbOnCanvas = Math.floor(this.widthThumbOnCanvas / this.thumbCache.width * this.thumbCache.height);
      this.rows = Math.floor(this.height / this.heightThumbOnCanvas);
      // this.heightThumbOnCanvas = Math.floor(height / this.rows);
    } while (this.columns * this.rows < this.length);
    this.rows = Math.floor(this.length + 1) / this.columns;

    this.drawAllThumb();
    // const dataUrl: string = this.canvas.toDataURL();
    // native.style.backgroundImage = `url('${dataUrl}')`;
  }

  constructor(
    private thumbCache: ThumbCache,
    private element: ElementRef
  ) {
  }

  ngOnInit() {
    this.thumb$Subscription = this.thumbCache.thumb$.subscribe(thumb => {
      this.drawThumb(thumb.id, thumb.canvas);
    });
  }

  ngOnDestroy() {
    this.thumb$Subscription.unsubscribe();
  }

  drawThumb(id: string, canvas: HTMLCanvasElement) {
    if (this._participantList == null) {
      return;
    }
    const position = this._participantList.id2position[id];
    if (position == null) {
      return;
    }
    const row = Math.floor(position / this.columns);
    const column = Math.floor(position - row * this.columns);

    const xOnCanvas = Math.floor(column / this.columns * this.width);
    const xOnCanvas1 = Math.floor((column + 1) / this.columns * this.width);
    const width = Math.min(this.widthThumbOnCanvas * 1.05 + 1, xOnCanvas1 - xOnCanvas);

    const yOnCanvas = Math.floor(row / this.rows * this.height);
    const yOnCanvas1 = Math.floor((row + 1) / this.rows * this.height);
    const height = Math.min(this.heightThumbOnCanvas * 1.05 + 1, yOnCanvas1 - yOnCanvas);

    const context: CanvasRenderingContext2D = this.canvas.getContext('2d');
    context.drawImage(canvas,
      0, 0, canvas.width, canvas.height,
      xOnCanvas, yOnCanvas, width, height);
  }

  drawAllThumb() {
    for (const id of Object.keys(this.thumbCache.cache)) {
      const cache = this.thumbCache.cache[id];
      this.drawThumb(id, cache.canvas);
    }
  }

}
