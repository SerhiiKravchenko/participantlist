import { Input, Component, OnInit } from '@angular/core';
import { Participant, ParticipantId } from '../../participant';
import { ChangeDetectionStrategy } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParticipantComponent implements OnInit {

  private _participant: Participant;
  imageBackgroundUrl: string;

  constructor(private changeDetector: ChangeDetectorRef) {
  }

  @Input()
  set participant(participant: Participant) {
    this._participant = participant;
    if (participant != null) {
      this._participant = participant;
    } else {
      this._participant = new Participant('');
    }
    this.imageBackgroundUrl = `url(${this._participant.picture})`;
  }

  @Input()
  visible: boolean;

  ngOnInit() {
  }

  selectItem($event) {

  }

  cssOnlineListItemClass() {
    let clazz = '';
    if (!this._participant.online) {
      clazz += 'offline ';
    }
  }

  cssOnlineIndicatorClass(): string {
    if (this._participant.online) {
      return 'online';
    } else {
      return 'offline';
    }
  }

}
