import { Component, OnInit } from '@angular/core';
import { Observer } from 'rxjs';
import { ParticipantSort, ParticipantSortBy, ParticipantSortByOrder } from '../../participant-sort';
import { Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-participant-sort',
  templateUrl: './participant-sort.component.html',
  styleUrls: ['./participant-sort.component.css']
})
export class ParticipantSortComponent implements OnInit, OnDestroy {

  constructor() { }

  @Input()
  participantSort$: BehaviorSubject<ParticipantSort>;
  participantSort: ParticipantSort;
  participantSortSubscription: Subscription;
  details = ParticipantSort.details;

  ngOnInit() {
    this.participantSortSubscription = this.participantSort$
      .distinctUntilChanged((a: ParticipantSort, b: ParticipantSort) => ParticipantSort.isEqual(a, b))
      .subscribe((p) => this.participantSort = p);
  }

  ngOnDestroy() {
    this.participantSortSubscription.unsubscribe();
  }

  onClickSort(sortBy: ParticipantSortBy) {
    const participantSort = ParticipantSort.from(this.participantSort);
    const ascending = participantSort.getSortDirection(sortBy);
    participantSort.add(new ParticipantSortByOrder(sortBy, !ascending));
    this.participantSort$.next(participantSort);
  }

}
