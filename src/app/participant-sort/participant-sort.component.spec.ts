import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantSortComponent } from './participant-sort.component';

describe('ParticipantSortComponent', () => {
  let component: ParticipantSortComponent;
  let fixture: ComponentFixture<ParticipantSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
