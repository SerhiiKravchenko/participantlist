import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ParticipantListVisualParameters } from '../../participant-list-visual-parameter';
import { Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-participant-list-visual-parameters',
  templateUrl: './participant-list-visual-parameters.component.html',
  styleUrls: ['./participant-list-visual-parameters.component.css']
})
export class ParticipantListVisualParametersComponent implements OnInit, OnDestroy {


  @Input()
  visualParameters$: BehaviorSubject<ParticipantListVisualParameters>;
  visualParameters: ParticipantListVisualParameters;
  subscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.subscription = this.visualParameters$.subscribe(visualParameters => {
      this.visualParameters = visualParameters;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onChange() {
    this.visualParameters$.next(this.visualParameters);
  }
}
