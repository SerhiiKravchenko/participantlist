import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantListVisualParametersComponent } from './participant-list-visual-parameters.component';

describe('ParticipantListVisualParametersComponent', () => {
  let component: ParticipantListVisualParametersComponent;
  let fixture: ComponentFixture<ParticipantListVisualParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantListVisualParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantListVisualParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
