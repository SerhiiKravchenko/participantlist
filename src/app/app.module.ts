import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { OnlineListComponent } from './online-list/online-list.component';
import { ParticipantServer } from '../participant-server';
import { ParticipantCache } from '../participant-cache';
import { ParticipantComponent } from './participant/participant.component';
import { ParticipantFilterComponent } from './participant-filter/participant-filter.component';
import { ParticipantSortComponent } from './participant-sort/participant-sort.component';
import { ThumbCache } from '../thumb-cache';
import { ThumbListComponent } from './thumb-list/thumb-list.component';
import { ParticipantListVisualParametersComponent } from './participant-list-visual-parameters/participant-list-visual-parameters.component';
import { enableProdMode } from '@angular/core';

enableProdMode();
@NgModule({
  declarations: [
    AppComponent,
    OnlineListComponent,
    ParticipantComponent,
    ParticipantFilterComponent,
    ParticipantSortComponent,
    ThumbListComponent,
    ParticipantListVisualParametersComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule, // <-- #2 add to @NgModule imports
    FormsModule
  ],
  providers: [ParticipantServer, ParticipantCache, ThumbCache],
  bootstrap: [AppComponent]
})
export class AppModule { }
