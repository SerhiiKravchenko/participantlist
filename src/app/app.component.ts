import { Component } from '@angular/core';
import { ParticipantCache } from '../participant-cache';
import { ParticipantServer } from '../participant-server';
import { BehaviorSubject } from 'rxjs';
import { ParticipantFilter } from '../participant-filter';
import { ParticipantSort } from '../participant-sort';
import { ThumbCache } from '../thumb-cache';
import { ElementRef } from '@angular/core';
import { OnInit } from '@angular/core';
import { ParticipantListVisualParameters } from '../participant-list-visual-parameter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  participantFilter$ = new BehaviorSubject(new ParticipantFilter());
  participantSort$ = new BehaviorSubject(new ParticipantSort());
  visualParameters$ = new BehaviorSubject(new ParticipantListVisualParameters());

  constructor(
    private server: ParticipantServer,
    private cache: ParticipantCache,
    private thumbCache: ThumbCache
  ) {

  }

  ngOnInit() {
    this.thumbCache.setThumbSize(32, 32);
  }

}
