import { Component, Input, OnInit } from '@angular/core';
import { ParticipantServer } from '../../participant-server';
import { ParticipantCache, RequestPriority } from '../../participant-cache';
import { Observable, BehaviorSubject } from 'rxjs';
import { ParticipantId, Participant, Participant$Visible } from '../../participant';
import { ParticipantFilter } from '../../participant-filter';
import { createBig, mergeUnique, createCSSSelector } from '../../utils';
import { combineLatestTree } from '../../combine-latest-tree';
import { ParticipantSort } from '../../participant-sort';
import { ParticipantList } from '../../participant-list';
import { ElementRef } from '@angular/core';
import { ThumbList } from '../../thumb-list';
import { ThumbCache } from '../../thumb-cache';
import { ChangeDetectionStrategy } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { ParticipantListVisualParameters } from '../../participant-list-visual-parameter';
import { Subscription } from 'rxjs/Subscription';
import { OnDestroy } from '@angular/core';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-online-list',
  templateUrl: './online-list.component.html',
  styleUrls: ['./online-list.component.css'],
})
export class OnlineListComponent implements OnInit, OnDestroy {

  width: string;
  height: string;
  scrollTop$ = new BehaviorSubject<number>(0);
  scrollTopDebounce$: Observable<number>;

  idList$ = new BehaviorSubject<ParticipantId[]>([]);
  idListNew$: Observable<ParticipantId[]>;
  idListCount$: Observable<number>;
  participant$list: Observable<BehaviorSubject<Participant>[]>;
  participant$listSorted$: Observable<BehaviorSubject<Participant>[]>;
  participantList$: Observable<ParticipantList>;
  participant$Visible$: Observable<Participant$Visible[]>;
  requestPriorityList$: Observable<RequestPriority[]>;
  requestPriorityList$subcription: Subscription;
  listFilteredCount$: Observable<number>;
  listFilteredOnlineCount$: Observable<number>;
  // listParticipantSorted$: Observable<BehaviorSubject<Participant>[]>;

  @Input()
  participantFilter$: Observable<ParticipantFilter>;
  @Input()
  participantSort$: Observable<ParticipantSort>;
  @Input()
  visualParameters$: Observable<ParticipantListVisualParameters>;
  visualParameters$subscription: Subscription;

  constructor(
    private server: ParticipantServer,
    private cache: ParticipantCache,
    private thumbCache: ThumbCache,
    private changeDetector: ChangeDetectorRef,
    public element: ElementRef
  ) {
  }

  trackByParticipant(index: number, p: Participant$Visible) {
    return p.participant$.value.id;
  }

  onScroll($event) {
    this.scrollTop$.next($event.target.scrollTop);
  }

  getElement(selector: string): HTMLElement {
    const element: HTMLElement = <HTMLElement>this.element.nativeElement.querySelector(selector);
    return element;
  }

  setWidth(width: number) {
    const newOnlinelist = this.getElement('.new-onlinelist');
    newOnlinelist.style.width = `${width}px`;
  }

  ngOnInit() {
    setTimeout(() => {// some timeout while debugger will attached
      this.scrollTopDebounce$ = this.scrollTop$
        .auditTime(300)
        .publishBehavior(0)
        .refCount();

      this.idListNew$ = this.server.requestOnlineList();
      this.idListNew$.withLatestFrom(this.idList$, (idListNew: ParticipantId[], idList: ParticipantId[]) => {
        const merged = mergeUnique(idListNew, idList);
        return merged;
      }).subscribe(this.idList$);

      this.idListCount$ = this.idList$.map(onlineList => {
        return onlineList.length;
      });

      this.participant$list = this.idList$.map(onlineList => {
        return this.cache.requestParticipants(onlineList);
      });

      this.participant$listSorted$ = this.participant$list.combineLatest(
        this.participantSort$.auditTime(200),
        this.participantFilter$.auditTime(200),
        (list: BehaviorSubject<Participant>[], sort: ParticipantSort, filter: ParticipantFilter) => {
          const listSorted: BehaviorSubject<Participant>[] = [];
          for (const p of list) {
            if (filter.isFiltered(p.value)) {
              listSorted.push(p);
            }
          }
          listSorted.sort((a: BehaviorSubject<Participant>, b: BehaviorSubject<Participant>) => {
            return sort.compare(a.value, b.value);
          });
          return listSorted;
        }
      ).share();

      this.participantList$ = Observable.combineLatest(
        this.participant$listSorted$, this.visualParameters$,
        (list: BehaviorSubject<Participant>[], visualParameters: ParticipantListVisualParameters) => {
          return new ParticipantList(list, visualParameters, this.scrollTopDebounce$);
        }
      ).share();

      this.participant$Visible$ = this.participantList$.map(list => list.list).share();

      this.requestPriorityList$ = Observable.combineLatest(
        this.participantList$, this.scrollTopDebounce$,
        (list: ParticipantList, scrollTop: number) => {
          const visualParameters = list.visualParameters;
          const middle = Math.floor((scrollTop + visualParameters.listHeight / 2) / visualParameters.participantHeight);
          const position = middle * visualParameters.columnCount;
          const requestPriorityList: RequestPriority[] = [];
          for (let i = 0; i < visualParameters.maxDistanceFromListCenterForDownload; i++) {
            const priority = 1 - i / visualParameters.maxDistanceFromListCenterForDownload;
            const pAbove = list.getParticipantByPosition(position + i + 1);
            const pBelow = list.getParticipantByPosition(position - i);
            if (pAbove) {
              requestPriorityList.push(new RequestPriority(pAbove.id, priority));
            }
            if (pBelow) {
              requestPriorityList.push(new RequestPriority(pBelow.id, priority));
            }
          }

          return requestPriorityList;
      });

      this.requestPriorityList$subcription = this.requestPriorityList$.subscribe(requestPriorityList => {
        for (const requestPriority of requestPriorityList) {
          this.cache.requestPriority(requestPriority);
        }
      });

      this.visualParameters$subscription = this.visualParameters$.subscribe(visualParameters => {
        const width = visualParameters.participantHeight * visualParameters.columnCount + visualParameters.scrollBarWidth + 10;
        this.width = `${width}px`;
        const height = visualParameters.listHeight;
        this.height = `${height}px`;
      });

      this.listFilteredCount$ = this.participantList$.map((list: ParticipantList) => {
        return list.list.length;
      });

      this.listFilteredOnlineCount$ = this.participantList$.map((list: ParticipantList) => {
        return list.online;
      });

      this.changeDetector.markForCheck();
    }, 1000);
  }

  ngOnDestroy() {
    this.requestPriorityList$subcription.unsubscribe();
    this.visualParameters$subscription.unsubscribe();
  }
}
