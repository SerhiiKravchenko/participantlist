import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantFilterComponent } from './participant-filter.component';

describe('ParticipantFilterComponent', () => {
  let component: ParticipantFilterComponent;
  let fixture: ComponentFixture<ParticipantFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
