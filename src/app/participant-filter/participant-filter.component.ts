import { Component, Input, OnInit, Output, OnChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ParticipantFilter } from '../../participant-filter';
import { BehaviorSubject } from 'rxjs';
import { Observer } from 'rxjs';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-participant-filter',
  templateUrl: './participant-filter.component.html',
  styleUrls: ['./participant-filter.component.css']
})
export class ParticipantFilterComponent implements OnInit, OnChanges {
  form: FormGroup;
  nameFilter: string;
  ageMinFilter: string;
  ageMaxFilter: string;
  nameChangeLog: string[] = [];

  @Input()
  participantFilter$: Observer<ParticipantFilter>;

  nameFilter$ = new BehaviorSubject<string>('');
  ageMinFilter$ = new BehaviorSubject<string>('');
  ageMaxFilter$ = new BehaviorSubject<string>('');

  constructor(
    private fb: FormBuilder) {

    this.createForm();
    this.logNameChange();
  }

  ngOnInit() {
    const nameControl = this.form.get('nameFilter');
    nameControl.valueChanges.subscribe(this.nameFilter$);
    const ageMinControl = this.form.get('ageMinFilter');
    ageMinControl.valueChanges.subscribe(this.ageMinFilter$);
    const ageMaxControl = this.form.get('ageMaxFilter');
    ageMaxControl.valueChanges.subscribe(this.ageMaxFilter$);

    Observable.combineLatest(
      this.nameFilter$,
      this.ageMinFilter$,
      this.ageMaxFilter$,
      (nameFilter: string, ageMinFilter: string, ageMaxFilter: string) => {
        const participantFilter = new ParticipantFilter({ name: nameFilter, ageMin: ageMinFilter, ageMax: ageMaxFilter });
        return participantFilter;
      }
    )
      .debounceTime(300)
      .subscribe(this.participantFilter$);
  }

  ngOnChanges() {
    this.form.reset({
      nameFilter: this.nameFilter,
      ageMinFilter: this.ageMinFilter,
      ageMaxFilter: this.ageMaxFilter
    });
  }

  createForm() {
    this.form = this.fb.group({
      nameFilter: '',
      ageMinFilter: '',
      ageMaxFilter: ''
    });
  }

  logNameChange() {
    const nameControl = this.form.get('nameFilter');
    nameControl.valueChanges.forEach(
      (value: string) => this.nameChangeLog.push(value)
    );
  }
}
