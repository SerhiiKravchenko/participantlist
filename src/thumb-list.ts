
import { ThumbCache } from './thumb-cache';
import { ParticipantList } from './participant-list';

export class ThumbList {
    canvas: HTMLCanvasElement;
    width: number;
    height: number;
    length: number;
    list: ParticipantList;

    columns: number;
    rows: number;
    widthThumbOnCanvas: number;
    heightThumbOnCanvas: number;

    constructor(private thumbCache: ThumbCache, list: ParticipantList, width: number, height: number) {
        this.list = list;
        this.length = list.list.length;
        this.width = width;
        this.height = height;

        this.canvas = <HTMLCanvasElement>document.createElement('canvas');
        this.canvas.width = width;
        this.canvas.height = height;
        const context: CanvasRenderingContext2D = this.canvas.getContext('2d');
        context.fillStyle = 'rgba(0, 0, 64, 0)';
        context.fillRect(0, 0, width, height);

        this.columns = 0;
        do {// iterate while find optimal number of columns and rows
            this.columns++;
            this.widthThumbOnCanvas = Math.floor(width / this.columns);
            this.heightThumbOnCanvas = Math.floor(this.widthThumbOnCanvas / thumbCache.width * thumbCache.height);
            this.rows = Math.floor(height / this.heightThumbOnCanvas);
            // this.heightThumbOnCanvas = Math.floor(height / this.rows);
        } while (this.columns * this.rows < this.length);
        this.rows = Math.floor(this.length + 1) / this.columns;
    }

    drawThumb(id: string, canvas: HTMLCanvasElement) {
        const position = this.list.id2position[id];
        if (position == null) {
            return;
        }
        const row = Math.floor(position / this.columns);
        const column = Math.floor(position - row * this.columns);

        const xOnCanvas = Math.floor(column / this.columns * this.width);
        const xOnCanvas1 = Math.floor((column + 1) / this.columns * this.width);
        const width = Math.min(this.widthThumbOnCanvas * 1.05 + 1, xOnCanvas1 - xOnCanvas);

        const yOnCanvas = Math.floor(row / this.rows * this.height);
        const yOnCanvas1 = Math.floor((row + 1) / this.rows * this.height);
        const height = Math.min(this.heightThumbOnCanvas * 1.05 + 1, yOnCanvas1 - yOnCanvas);

        const url = 'https://d1mti8cqxh4eqy.cloudfront.net/10f2bc03-8411-4eee-a9c3-3483f3771437.jpeg';

        const context: CanvasRenderingContext2D = this.canvas.getContext('2d');
        context.drawImage(canvas,
            0, 0, canvas.width, canvas.height,
            xOnCanvas, yOnCanvas, width, height);
    }

    drawAllThumb() {
        for (const id of Object.keys(this.thumbCache.cache)) {
            const cache = this.thumbCache.cache[id];
            this.drawThumb(id, cache.canvas);
        }
    }

    getDataUrl(): string {
        const dataUrl: string = this.canvas.toDataURL();
        return dataUrl;
    }
}
