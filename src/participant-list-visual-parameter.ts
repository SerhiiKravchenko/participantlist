
export class ParticipantListVisualParameters {
    columnCount = 2;
    listHeight = 700;
    participantWidth = 141;
    participantHeight = 141;
    scrollBarWidth = 120;
    maxDistanceFromListCenterForDownload = 200;
    maxDistanceFromEdgeForVisibility = 20;
}
